//
// Created by maciej on 11/28/20.
//

#include <string>
#include "WareType.h"

#pragma once

class Ware {
private:
    int _typeId;
    int _quantity;

public:

    explicit Ware(const WareType& type, int quantity=0);
    explicit Ware(int typeId,int quantity=0);
    void modifyQuantity(int difference);
    [[nodiscard]] int getQuantity() const;
    [[nodiscard]] std::string getName() const;
    [[nodiscard]] int getId() const;
    Ware* takeQuantity(int quantity);
};

