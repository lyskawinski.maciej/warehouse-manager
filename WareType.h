//
// Created by maciej on 11/29/20.
//

#pragma once
#include "string"
#include "map"

class WareType {
private:
    int _id;
    std::string _name;
    static int globalIdCounter;
    static std::map<int,WareType*> _wareTypeRegistry; // I am aware that vector / array solution would be potentially faster or be more memory efficient but using map is less of a hustle

    explicit WareType(std::string name,int id);
public :

    [[nodiscard]] int getId() const ;
    [[nodiscard]] std::string getName() const;



    static WareType getWareTypeById(int id);
    static WareType getWareTypeByName(const std::string& name);

    class Factory {
    public:
        static WareType create(const std::string& name);
        static void clean();
    };

    static void getAllWares(std::ostream& ostream);
};



