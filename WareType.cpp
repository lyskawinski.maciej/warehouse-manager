//
// Created by maciej on 11/29/20.
//

#include "WareType.h"
#include "exceptions/WareTypeNonExistentException.h"

#include <utility>
#include <iomanip>

int WareType::globalIdCounter = 0;
std::map<int, WareType *> WareType::_wareTypeRegistry;

WareType::WareType(std::string name, int id) : _name(std::move(name)), _id(id) {
}

int WareType::getId() const {
    return _id;
}

std::string WareType::getName() const {
    return _name;
}

WareType WareType::getWareTypeById(int id) {
    auto pointer = _wareTypeRegistry[id];
    if (!pointer)
        throw WareTypeNonExistentException(id);
    return *pointer;
}

WareType WareType::getWareTypeByName(const std::string &name) {
    WareType *pointer = nullptr;
    for (auto pair : _wareTypeRegistry) {
        if (pair.second->getName() == name) {
            pointer = pair.second;
            break;
        }
    }
    if (!pointer)
        throw WareTypeNonExistentException(name);
    return *pointer;
}

void WareType::getAllWares(std::ostream &stream) {
    stream << std::setw(3) << std::left << "ID" << std::setw(0) << "|"  << "Names" << std::endl;
    stream << std::setw(54) << std::setfill('-') << "" << std::endl << std::setfill(' ');
    for (auto warePair : _wareTypeRegistry) {
        auto ware = warePair.second;
        stream << std::setw(3) << ware->getId() << "|" << ware->getName()  << std::endl;
    }
}

WareType WareType::Factory::create(const std::string &name) {
    return *(_wareTypeRegistry[globalIdCounter++] = new WareType(name, globalIdCounter));
}

void WareType::Factory::clean() {
    for (auto wareType :_wareTypeRegistry)
        delete wareType.second;
}
