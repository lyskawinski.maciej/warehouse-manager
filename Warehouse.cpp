//
// Created by maciej on 11/28/20.
//

#include <iomanip>
#include "Warehouse.h"
#include "exceptions/WareNotPresentException.h"


Ware *Warehouse::getWareByName(const std::string &name) {
    for (auto pair : _wares) {
        if (pair.second->getName() == name)
            return pair.second;
    }
    return nullptr;
}

Ware *Warehouse::getWareById(int id) {
    if (_wares.count(id))
        return _wares.at(id);
    return nullptr;
}

Ware *Warehouse::acceptWare(Ware *ware) {
    if (!_wares.count(ware->getId())) {
        auto type = WareType::getWareTypeById(
                ware->getId()); //Fetching the type is intentional to check if the type exists
        _wares[type.getId()] = ware;
        return ware;
    }
    auto existentWare = _wares[ware->getId()];
    existentWare->modifyQuantity(ware->getQuantity());
    return existentWare;
}

Ware *Warehouse::takeWareById(int id, int quantity) {
    if (!_wares.count(id))
        throw WareNotPresentException(id);
    _wares[id]->modifyQuantity(-quantity);
    if(_wares[id]->getQuantity() ==0) {
        delete _wares[id];
        _wares.erase(id);
    }
    return new Ware(id, quantity);
}

Ware *Warehouse::takeWareByName(const std::string &name, int quantity) {
    auto type = WareType::getWareTypeByName(name);
    return takeWareById(type.getId(), quantity);
}

void Warehouse::getContent(std::ostream &stream) {
    stream << std::setw(3) << std::left << "ID" << std::setw(0) << "|" << std::setw(50) << "Names" << std::setw(0)
           << "|" << "Quantity" << std::endl;
    stream << std::setw(67) << std::setfill('-') << "" << std::endl << std::setfill(' ');
    for (auto warePair : _wares) {
        auto ware = warePair.second;
        stream << std::setw(3) << ware->getId() << "|" << std::setw(50) << ware->getName() << "|"
               << ware->getQuantity() << std::endl;
    }
}

int Warehouse::getWareCount() {
    return _wares.size();
}

Warehouse::~Warehouse() {
    for(auto warePair: _wares)
        delete warePair.second;
}
