# Warehouse manager

## Goals

### Original 

##### Conceptual
 - Adding ware types to a warehouse
 - Changing the amount of stored ware in the warehouse
 - Protection against over-withdrawal
 
 
##### Programmatic 
 - `Ware` class that will have `ID`, `name` and `quantity`
 - Automatic incrementation of `ID` of `Ware`
 - `Warehouse` class that will keep all the `Wares` in a `std::vector`
 
 
### Adapted

##### Conceptual
- Central system of ware types
- Warehouses can store wares of given types
- Ability to change the amount of stored ware in the warehouse
- Protection against over-withdrawal

##### Programmatic
 - `WareType` class that will have `ID` and  `name`
 - `Ware` class that will have `typeId` and `quantity`
 - Automatic incrementation of `ID` of `WareType`
 - `Warehouse` class that will keep all the `Wares` in a `std::map` where key is the `ID` of the `WareType` 
 
 
 
 ## Usage
 
 ### Interface
 
 Text interface based on numerical and text answers to asked questions. One main interface and subinterfaces based on choice
 
 #### Main interface
 ```
     Which option you want to choose?
 0)  Exit warehouse manager
 1)  Create new ware type
 2)  Add new ware to the warehouse
 3)  Take a ware our of the warehouse
 4)  Display contents of the warehouse
 5)  Show all ware types
 ```

Answer in form of a integer number from zero to five


#### Subinterfaces

##### Exit option

Quits the program

##### Type creation interface

Question about the name of the type that should be created (text)

##### Add ware interface

Question about the id of the type of ware that should be stored in the warehouse (integer number)

Question about the amount of ware that should be stored (integer number)

##### Take ware interface

Question about the id of the type of ware that should be taken from the warehouse (integer number)

Question about the amount of ware that should be taken (integer number)

##### Display contents option

Shows all the wares (id, name, quantity) that are currently stored in the warehouse

##### Show types option

Show all the ware types (id, name)


## Potential features

- Handling of multiple warehouses
- Managing wares using names along with id (Only text interface needed) - Use regex instead of full name?
