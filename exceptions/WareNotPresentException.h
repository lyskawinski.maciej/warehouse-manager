//
// Created by maciej on 11/29/20.
//

#pragma once
#include "string"

class WareNotPresentException : std::exception{
public:
    explicit WareNotPresentException(int id);
    explicit WareNotPresentException(const std::string& name);
};

