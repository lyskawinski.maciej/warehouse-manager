//
// Created by maciej on 11/29/20.
//

#pragma once
#include "string"

class WareTypeNonExistentException : std::exception{
public:
    explicit WareTypeNonExistentException(int id);
    explicit WareTypeNonExistentException(const std::string& name);
};

