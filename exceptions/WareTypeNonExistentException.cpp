//
// Created by maciej on 11/29/20.
//
#include "iostream"
#include "WareTypeNonExistentException.h"
WareTypeNonExistentException::WareTypeNonExistentException(const std::string& name) {
    std::cerr << "Ware type with name " << name << " does not exist"<<std::endl;
}

WareTypeNonExistentException::WareTypeNonExistentException(int id) {
    std::cerr << "Ware type with id " << id << " does not exist"<<std::endl;

}

