//
// Created by maciej on 11/29/20.
//
#include "iostream"
#include "WareNotPresentException.h"
WareNotPresentException::WareNotPresentException(const std::string& name) {
    std::cerr << "Ware with name " << name << " is not present in this ware house"<<std::endl;
}

WareNotPresentException::WareNotPresentException(int id) {
    std::cerr << "Ware with id " << id << " is not present in this ware house"<<std::endl;
}

