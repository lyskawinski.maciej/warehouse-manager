//
// Created by maciej on 11/28/20.
//

#include "NegativeQuantityException.h"

NegativeQuantityException::NegativeQuantityException(int invalidQuantity) {
    std::cerr << "Quantity can't be negative : " << invalidQuantity<<std::endl;
}

