//
// Created by maciej on 11/28/20.
//

#pragma once

#include <exception>
#include <iostream>

class NegativeQuantityException : std::exception {
public:
    explicit NegativeQuantityException(int invalidQuantity);
};



