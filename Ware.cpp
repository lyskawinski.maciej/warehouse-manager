//
// Created by maciej on 11/28/20.
//

#include "Ware.h"

#include <utility>
#include "exceptions/NegativeQuantityException.h"

void Ware::modifyQuantity(int difference) {
    int newQuantity = _quantity + difference;
    if (newQuantity < 0)
        throw NegativeQuantityException(newQuantity);
    _quantity = newQuantity;
}

int Ware::getQuantity() const {
    return _quantity;
}

Ware *Ware::takeQuantity(int quantity) {
    auto newWare = new Ware(_typeId, quantity);
    modifyQuantity(-quantity);
    return newWare;
}

Ware::Ware(int typeId, int quantity) : _quantity(quantity), _typeId(typeId) {

}

Ware::Ware(const WareType &type, int quantity) : Ware(type.getId(), quantity) {

}

std::string Ware::getName() const {
    return WareType::getWareTypeById(_typeId).getName();
}

int Ware::getId() const {
    return _typeId;
}
