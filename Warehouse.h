//
// Created by maciej on 11/28/20.
//

#pragma once

#include <vector>
#include <map>
#include "Ware.h"

class Warehouse {
private:
    std::map<int,Ware*> _wares;
    ~Warehouse();
public:
    Ware* getWareByName(const std::string& name);
    Ware* getWareById(int id);
    Ware* acceptWare(Ware* ware);
    Ware* takeWareById(int id,int quantity);
    Ware* takeWareByName(const std::string& name,int quantity);
    [[nodiscard]] int getWareCount();


    void getContent(std::ostream& stream);

};