#include <iostream>
#include "exceptions/WareTypeNonExistentException.h"
#include "Warehouse.h"
#include "exceptions/WareNotPresentException.h"
#include "exceptions/NegativeQuantityException.h"

#define READ_VALUE(VALUE) while(VALUE < 0) {\
try {\
int uncheckedValue = readInt();\
VALUE = uncheckedValue;\
}\
catch (const std::exception&) {\
std::cout<<"Please provide a valid value for "<< #VALUE<<std::endl;\
}\
}

#define READ_VALUE_RANGE(VALUE,MAX) while(VALUE < 0) {\
try {\
int uncheckedValue = readInt();\
if (uncheckedValue < 0 || uncheckedValue > MAX)\
throw std::exception();\
VALUE = uncheckedValue;\
}\
catch (const std::exception&) {\
std::cout<<"Please provide a valid value for "<< #VALUE <<" (0-"<<MAX<<")"<<std::endl;\
}\
}

enum InterfaceOption {
    Exit = 0,
    CreateType = 1,
    AddWare = 2,
    TakeWare = 3,
    PrintWarehouse = 4,
    PrintTypes = 5,
};


InterfaceOption getOption();
int readInt();
void processOption(InterfaceOption option);


void createType();



void addWare();
void takeWare();
void printWarehouse();
void printAllTypes();


Warehouse* warehouse;

int main() {
    warehouse = new Warehouse();
    InterfaceOption option = Exit;

    while((option = getOption()))
        processOption(option);

    WareType::Factory::clean();
}

InterfaceOption getOption()
{
    std::cout.fill(' ');

    std::cout.width(4);
    std::cout<<"";
    std::cout.width(0);
    std::cout<<"Which option you want to choose?"<<std::endl;

    std::cout.width(4);
    std::cout<<std::left<<"0)";
    std::cout.width(0);
    std::cout<<"Exit warehouse manager"<<std::endl;

    std::cout.width(4);
    std::cout<<std::left<<"1)";
    std::cout.width(0);
    std::cout<<"Create new ware type"<<std::endl;

    std::cout.width(4);
    std::cout<<std::left<<"2)";
    std::cout.width(0);
    std::cout<<"Add new ware to the warehouse"<<std::endl;

    std::cout.width(4);
    std::cout<<std::left<<"3)";
    std::cout.width(0);
    std::cout<<"Take a ware our of the warehouse"<<std::endl;

    std::cout.width(4);
    std::cout<<std::left<<"4)";
    std::cout.width(0);
    std::cout<<"Display contents of the warehouse"<<std::endl;

    std::cout.width(4);
    std::cout<<std::left<<"5)";
    std::cout.width(0);
    std::cout<<"Show all ware types"<<std::endl;
    int option = -1;
    READ_VALUE_RANGE(option,5);
    return static_cast<InterfaceOption>(option);
}

int readInt(){
    std::string rawValue;
    std::getline(std::cin, rawValue);
    return std::stoi(rawValue);
}

void processOption(InterfaceOption option){
    std::cout<<std::endl;
    switch (option) {
        case Exit:
            return;
        case CreateType:
            createType();
            break;
        case AddWare:
            addWare();
            break;
        case TakeWare:
            takeWare();
            break;
        case PrintWarehouse:
            printWarehouse();
            break;
        case PrintTypes:
            printAllTypes();
            break;
    }
    std::cout<<std::endl;
}
void createType() {
    std::cout<< "What will be the name of the new type?"<<std::endl;
    std::string name;
    std::getline(std::cin,name);
    WareType::Factory::create(name);
}

void addWare() {
    std::cout<< "Of what type?"<<std::endl;
    int id = -1;
    READ_VALUE(id)
    try {
        WareType::getWareTypeById(id);
    } catch (const WareTypeNonExistentException&) {
        return;
    }
    std::cout<< "How much you want to add?"<<std::endl;
    int quantity = -1;
    READ_VALUE(quantity)
    if(quantity < 0){
        std::cout <<"Quantity can't be negative"<<std::endl;
        return;
    }
    warehouse->acceptWare(new Ware(id,quantity));
}

void takeWare() {
    std::cout<< "Which ware you want to take?"<<std::endl;
    int id = -1;
    READ_VALUE(id)
    std::cout<< "How much you want to take?"<<std::endl;
    int quantity = -1;
    READ_VALUE(quantity)
    try {
        warehouse->takeWareById(id,quantity);
    }
    catch (const WareNotPresentException&) {
    }
    catch (const NegativeQuantityException&) {
    }
}

void printWarehouse() {
    warehouse->getContent(std::cout);
}

void printAllTypes() {
   WareType::getAllWares(std::cout);
}